import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;


public class David implements Jugador {

	private int col = 8;
	private int prof = 8;
	public David() {

	}

	public David(int prof) {
		this.prof = prof;
	}

	@Override
	public int moviment(Tauler t, int color) {
		int max[] = new int [2];
		max[0] = Integer.MIN_VALUE;
		max[1] = Integer.MIN_VALUE;
		AnimalTable at = new AnimalTable(t);
		int valor;
		for(int accio : accions(at)) {
			if (at.isSolution(accio, color)) return accio;
			else if (at.isSolution(accio, color * -1)) return accio;
			at.push(accio, color);
			if (at.isSolution(accio, color * -1)) valor = Integer.MIN_VALUE;
			else valor = alfaBeta(at, color, Integer.MIN_VALUE, Integer.MAX_VALUE, prof - 1);
//			System.out.println("## Tirada columna " + accio + ": " + valor);
			at.pop(accio);
			if(max[1] < valor) {
				max[0] = accio;
				max[1] = valor;
			}
		}

//		System.out.println("ALFABETA COL: " + max[0]);

		return max[0];
	}

	@Override
	public String nom() {
		return "Animal Instinct";
	}

	private int alfaBeta(AnimalTable t, int color, int a, int b, int prof) {
		if(prof == 0) return eval(t, color);
		else {
			for(int accio : accions(t)) {

				t.push(accio, color);
				a = Math.max(a, alfaBeta(t, color * -1, b * -1, a * -1, prof - 1) * -1);
				t.pop(accio);
				if(b <= a) break;
			}
			return a;
		}
	}

	/**
	 * Retorna les accions a les columnes on es puguin encara afegir peces
	 * @param t Tauler actual del joc
	 * @return ArrayList de les possibles accions a les columnes 
	 */
	private ArrayList<Integer> accions(AnimalTable t) {
		ArrayList<Integer> accions = new ArrayList<Integer>();
		for (int i = 0; i < col; i++) {
			if(t.movpossible(i)) accions.add(i);
		}
		return accions;
	}

	/**
	 * Funcio que pondera una fulla de l'alfabeta
	 * @param t Tauler de la fulla
	 * #param color respecte el qual s'ha d'evaluar
	 * @return ponderacio
	 */
	private int eval(AnimalTable t, int color) {
		return t.searchThreat(color);
	}
}
