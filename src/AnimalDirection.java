import java.awt.Point;
import java.util.Iterator;
import java.util.LinkedList;


public class AnimalDirection {
	private LinkedList<Point> coord;
	private Iterator<Point> iter;
	private boolean[] visited;
	private int counterVisited;
	
	public AnimalDirection(Point p) {
		coord = new LinkedList<Point>();
		if(p.getX() != 7 && p.getY() != 7) {
			coord.add(new Point(0,1));
			coord.add(new Point(1,1));
			coord.add(new Point(1,0));
			if(p.getY() != 0) coord.add(new Point(1,-1));
		}
		else if (p.getY() == 7 && p.getX() != 7) {
			coord.add(new Point(1,0));
			coord.add(new Point(1,-1));
		}
		else if (p.getX() == 7 && p.getY() != 7) coord.add(new Point(0,1));
		
		visited = new boolean[coord.size()];
		for (int i = 0; i < visited.length; i++) visited[i] = false;
		
		iter = coord.iterator();
		counterVisited = -1;
	}
	
	/**
	 * Posiciona la direccio actual a l'inici de totes les direccions
	 */
	public void start() {
		counterVisited = -1;
		iter = coord.iterator();
	}
	
	/**
	 * Determina si existex una direccio encara per recorrer
	 * @return cert si queda una direccio per recorrer
	 */
	public boolean hasNext() {
		return iter.hasNext();
	}
	
	/**
	 * Obtenir una direccio i avan�ar a la seguent disponible 
	 * @return Point amb l'imencrement corresponent a la direccio actual
	 */
	public Point next() {
		counterVisited++;
		return iter.next();
	}
	
	/**
	 * Determina si la direccio actual ha estat visitada anteriorment
	 * @return cert si la direccio actual ha estat visitada
	 */
	public boolean isVisited() {
		return visited[counterVisited];
	}
	
	/**
	 * L'estat de "visitat" de la direccio actual pren per valor el bolea que es passa per param
	 * @param boolea per dir si la direccio actual ha estat visitada o no
	 */
	public void setVisited(boolean b) {
		visited[counterVisited] = b;
	}
}
