import java.awt.Point;


public class AnimalSquare {
	private int color;
	private AnimalDirection directions;

	public AnimalSquare(int color, int x, int y) {
		this.color = color;
		directions = new AnimalDirection(new Point(x, y));		
	}
	
	/**
	 * Comunica a l'AnimalDirection de l'square que posicini la direccio actual a l'inici de totes les direccions
	 */
	public void startDirection() {
		directions.start();
	}
	
	/**
	 * Demana a l'AnimalDirection de l'square que determini si existex una direccio encara per recorrer
	 * @return cert si queda una direccio per recorrer
	 */
	public boolean hasMoreDirections() {
		return directions.hasNext();
	}
	
	/**
	 * Demana a l'AnimalDirection de l'square que obtingui una direccio i avan�ar a la seguent disponible 
	 * @return Point amb l'imencrement corresponent a la direccio actual
	 */
	public Point nextDirection() {
		return directions.next();
	}
	
	/**
	 * Obtenir color corresponent a l'square
	 * @return el color de l'square
	 */
	public int getColor() {
		return color;
	}
	
	/**
	 * Determina si l'square esta buit
	 * @return cert si l'square esta buit, false en cas contrari
	 */
	public boolean empty() {
		return color == 0;
	}
	
	/**
	 * El color de l'square pren per valor el color s'ha passat per param
	 * @param color desitjat per a l'square (-1, 1 o 0)
	 */
	public void setColor(int color) {
		this.color = color;
	}
	
	/**
	 * Demana a l'AnimalDirection de l'square que determina si la direccio actual ha estat visitada anteriorment
	 * @return cert si la direccio actual ha estat visitada
	 */
	public boolean isDirectionVisited() {
		return directions.isVisited();
	}
	
	/**
	 * Comunica a l'AnimalDirection de l'square que l'estat de "visitat" de la direccio actual pren per valor el bolea que es passa per param
	 * @param boolea per dir si la direccio actual ha estat visitada o no
	 */
	public void setDirectionAsVisited(boolean b) {
		directions.setVisited(b);
	}
}
