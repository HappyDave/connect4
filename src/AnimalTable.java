import java.awt.Point;



public class AnimalTable {
	private AnimalSquare[][] table;
	private int[] cols;
	private int count;

	public AnimalTable(Tauler t) {
		table = new AnimalSquare[t.getMida()][t.getMida()];
		count = 0;
		cols = new int[t.getMida()];
		for (int i = 0; i < cols.length; i++) cols[i] = 0;
		
		for (int i = 0; i < t.getMida(); i++) {
			for (int j = 0; j < t.getMida(); j++) {
				int color = t.getColor(i, j);
				if(color != 0) {
					count++;
					cols[j]++;
				}
				table[i][j] = new AnimalSquare(t.getColor(i, j), j, i);
			}
		}
	}

	/**
	 * Introdueix una fitxa en la columna indicada
	 * @param columna
	 * @param color
	 * @return cert si el proces ha sigut satisfactori
	 */
	public boolean push(int col, int color){
		boolean push = cols[col] < table.length;
		if(push) {
			table[cols[col]][col].setColor(color);
			cols[col]++;
			count++;
		}
		return push;
	}

	/**
	 * Extreu una fitxa de la columna indicada
	 * @param columna
	 */
	public void pop(int col){
		if(cols[col] > 0) {
			table[cols[col] - 1][col].setColor(0);
			cols[col]--;
			count--;
		}
	}

	/**
	 * Determina si queda algun moviment possible a la columna indicada
	 * @param columna
	 * @return cert si existeix algun moviment possible
	 */
	public boolean movpossible(int col) {
		return table[table[col].length - 1][col].empty();
	}

	/**
	 * Determina si l'AnimalTable esta ple de fitxes
	 * @return cert si no hi cap fitxa
	 */
	public boolean espotmoure() {
		return this.count < table.length * table.length;
	}
	
	/**
	 * Determina si ficant una fitxa en la columna indicada es podria produir un 4 en linia
	 * @param columna
	 * @param color
	 * @return cert si es solucio
	 */
	public boolean isSolution(int col, int color) {
		boolean solution = false;
		AnimalDirection directions = new AnimalDirection(new Point(3,3));
		directions.start();
		Point n = new Point(col, cols[col]);
		while(directions.hasNext() && !solution) {
			Point p = directions.next();						
			solution = (counterColor(n, p, color) + counterColor(n, new Point((int)p.getX() * -1, (int)p.getY() * -1), color) >= 3);
		}
		return solution;
	}

	/**
	 * Determina quantes fitxes del mateix color hi ha en la direcci� p respecte n
	 * @param n Point inicial
	 * @param p Point corresponent a la direcci� que es vol consultar
	 * @param color
	 * @return el numero de fitxes del mateix color en la direcci� p amb origen n
	 */
	private int counterColor(Point n, Point p, int color) {
		int token = 0;
		Point paux = new Point(n);
		paux.translate((int)p.getX(), (int)p.getY());
		while(inRange(paux) && getSquare(paux).getColor() == color && token <= 3) {
			token++;
			paux.translate((int)p.getX(), (int)p.getY());
		}
		return token;
	}

	/**
	 * Obtenir la ponderacio total corresponent al sumatori de tots els "perills" en l'AnimalTable
	 * @param color
	 * @return ponderacio total
	 */
	public int searchThreat(int color) {
		int i, pond = 0;
		for (int j = 0; j < table.length; j++) {
			i = 0;

			while(i < table.length && !table[i][j].empty()) {
				int auxcolor = table[i][j].getColor();
				table[i][j].startDirection();

				while(table[i][j].hasMoreDirections()) {
					Point p = table[i][j].nextDirection();

					if(table[i][j].isDirectionVisited()) {
						table[i][j].setDirectionAsVisited(false);
					}
					else {
						Point n = new Point(j, i);
						n.translate((int)p.getX(), (int)p.getY());
						int token = 1;

						while(inRange(n) && getSquare(n).getColor() == auxcolor) {
							boolean trobat = false;
							token++;

							getSquare(n).startDirection();

							while(getSquare(n).hasMoreDirections() && !trobat) {
								trobat = getSquare(n).nextDirection().equals(p);
							}

							if(trobat) getSquare(n).setDirectionAsVisited(true);

							n.translate((int)p.getX(), (int)p.getY());

						}

						if (token == 2) {
							n.translate((int)p.getX(), (int)p.getY());
							int ctra = contraEmpties(j, i, p);
							if (inRange(n) && getSquare(n).empty() && ctra >= 1 || ctra >=2) {
								if(auxcolor == color) pond += 100;
								else pond -= 200;
							}
						}
						else if (token == 3) {
							if (inRange(n) && getSquare(n).empty()) {
								if(auxcolor == color) pond += 100;
								else pond -= 180;
							}
							else {
								if (contraEmpties(j, i, p) >= 1) {
									if(auxcolor == color) pond += 100;
									else pond -= 180;
								}
							}

						}
					}
				}
				i++;
			}
		}
		return pond;
	}

	/**
	 * Determina els espais buits darrera la posicio corresponent al Point(j,i) en la direcci� p
	 * @param j posicio X
	 * @param i posicio Y
	 * @param p direccio
	 * @return numero d'espais buits
	 */
	private int contraEmpties(int j, int i, Point p) {
		int ret = 0;
		Point paux= new Point(j + (int)p.getX() * -1, i + (int)p.getY() * -1);
		while(inRange(paux) && getSquare(paux).empty() && ret < 3) {
			paux = new Point(paux.x + (int)p.getX() * -1, paux.y + (int)p.getY() * -1);
			ret++;
		}
		return ret;
	}

	/**
	 * Obtenir l'square corresponent al p:Point
	 * @param p Point
	 * @return retorna l'square
	 */
	private AnimalSquare getSquare(Point p) {
		return table[(int)p.getY()][(int)p.getX()];
	}

	/**
	 * Determina si un Point es troba dins de la mida de l'AnimalTable
	 * @param p Point
	 * @return cert si el Point esta dins de l'AnimalTable
	 */
	private boolean inRange(Point p) {
		return p.getX() >= 0 && p.getX() < table.length && p.getY() >= 0 && p.getY() < table.length;
	}

	/**
	 * Imprimeix per pantalla l'AnimalTable
	 */
	public void printTable(){
		for (int i = table.length - 1; i >= 0; i--) {
			for (int j = 0; j < table.length; j++) {
				if(table[i][j].getColor() == -1) System.out.print(table[i][j].getColor()); 
				else System.out.print(" " + table[i][j].getColor());
			}
			System.out.println();
		}
	}
}
